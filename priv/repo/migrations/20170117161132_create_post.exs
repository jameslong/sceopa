defmodule Sceopa.Repo.Migrations.CreatePost do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :title, :text
      add :content, :text
      add :public, :boolean
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end
  end
end
