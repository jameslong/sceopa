defmodule Sceopa.Blog do
  @moduledoc "Context for blog related functionality."

  alias Ecto.Multi
  alias Sceopa.Blog.Post
  alias Sceopa.Repo

  def post_changeset(post \\ %Post{}, params \\ %{}) do
    Post.changeset(post, params)
  end

  def get_post(id) do
    Repo.get(Post, id)
  end

  def user_posts(user) do
    Repo.all(Ecto.assoc(user, :posts))
  end

  def author?(_post, nil), do: false
  def author?(post, user) do
    post.user_id == user.id
  end

  def public?(post), do: post.public

  def visible?(post, nil), do: public?(post)
  def visible?(post, user) do
    author?(post, user) or public?(post)
  end

  def visible_posts(posts, user) do
    Enum.filter(posts, &visible?(&1, user))
  end

  def import_posts(user, file) do
    with {:ok, contents} <- File.read(file.path),
         {:ok, %{"posts" => posts}} <- Poison.decode(contents),
         {:ok, _results} <-
           posts
           |> Enum.map(fn params ->
             assoc = Ecto.build_assoc(user, :posts)
             Post.changeset(assoc, params)
           end)
           |> Enum.with_index()
           |> Enum.reduce(Multi.new(), fn {changeset, index}, multi ->
             Multi.insert(multi, index, changeset)
           end)
           |> Repo.transaction(),
             do: {:ok}
  end

  def to_json(post) do
    Map.take(post, [:title, :content, :public, :inserted_at, :updated_at])
  end

  def insert_post(user, post_params) do
    user
    |> Ecto.build_assoc(:posts)
    |> Post.changeset(post_params)
    |> Repo.insert()
  end

  def update_post(post, post_params) do
    post
    |> post_changeset(post_params)
    |> Repo.update()
  end

  def delete_post(post) do
    Repo.delete(post)
  end
end
