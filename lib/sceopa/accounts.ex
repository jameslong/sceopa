defmodule Sceopa.Accounts do
  @moduledoc "Context for account related functionality."

  alias Sceopa.Accounts.User
  alias Sceopa.Email
  alias Sceopa.Repo
  alias Sceopa.Web.Router.Helpers

  def registration_changeset(user \\ %User{}, params \\ %{}) do
    User.registration_changeset(user, params)
  end

  def user_changeset(user \\ %User{}, params \\ %{}) do
    User.changeset(user, params)
  end

  def username_changeset(user \\ %User{}, params \\ %{}) do
    User.username_changeset(user, params)
  end

  def password_changeset(user \\ %User{}, params \\ %{}) do
    User.password_changeset(user, params)
  end

  def get_user(id) do
    Repo.get(User, id)
  end

  def get_user_by_email(email) do
    Repo.get_by(User, email: email)
  end

  def get_user_by_username(username) do
    Repo.get_by(User, username: username)
  end

  def update_username(user, params) do
    user
    |> username_changeset(params)
    |> Repo.update()
  end

  def update_password(user, params) do
    user
    |> password_changeset(params)
    |> Repo.update()
  end

  def create_account(user_params) do
    reg_changeset = registration_changeset(%User{}, user_params)
    case Repo.insert(reg_changeset) do
      {:ok, user} ->
        url = gen_email_verify_url(user)
        Email.send_verification(user, url)
        {:ok, user}
      {:error, reg_changeset} -> {:error, reg_changeset}
    end
  end

  defp gen_email_verify_url(user) do
    %{username: username, email: email, email_verification_key: key} = user
    Helpers.email_url(Sceopa.Endpoint, :index,
      username: username, email: email, key: key)
  end

  def verify_email_key(username, key) do
    case get_user_by_username(username) do
      user = %{email_verification_key: ^key} ->
        {:ok, user}
      _ ->
        {:error}
    end
  end

  def delete_account(user) do
    Repo.delete(user)
  end
end
