defmodule Sceopa.Web.UserSettingsPasswordController do
  use Sceopa.Web, :controller

  alias Sceopa.Accounts
  alias Sceopa.Auth
  import Sceopa.Web.Plugs.Auth
  plug :authenticate

  def edit(conn, _params) do
    user = conn.assigns.current_user
    changeset = Accounts.user_changeset(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"user" => user_params}) do
    user = conn.assigns.current_user

    case Auth.verify_pass(user, user_params["current_password"]) do
      {:ok, _user} ->
        case Accounts.update_password(user, user_params) do
          {:ok, _user} ->
            conn
            |> put_flash(:info, "Account updated successfully.")
            |> redirect(to: user_settings_path(conn, :index))
          {:error, changeset} ->
            render(conn, "edit.html", user: user, changeset: changeset)
        end
      _ ->
        changeset = Accounts.password_changeset(user, user_params)
        conn
        |> put_flash(:error, "Incorrect current password. Please try again.")
        |> render("edit.html", user: user, changeset: changeset)
    end
  end
end
