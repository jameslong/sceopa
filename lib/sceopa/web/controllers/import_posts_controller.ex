defmodule Sceopa.Web.ImportPostsController do
  @moduledoc """
  Provides action for uploading posts.
  """

  use Sceopa.Web, :controller
  import Sceopa.Web.Plugs.Auth
  import Sceopa.Web.Plugs.User

  alias Sceopa.Blog

  plug :authenticate
  plug :ensure_user

  def index(conn, _params) do
    user = conn.assigns.current_user
    render(conn, "index.html", user: user)
  end

  def update(conn, %{"file" => file}) do
    user = conn.assigns.current_user

    case Blog.import_posts(user, file) do
      {:ok} ->
        conn
        |> put_flash(:info, "Posts successfully imported!")
        |> redirect(to: dashboard_path(conn, :index))
      _ ->
        conn
        |> put_flash(:error, "There was an error importing your posts,\
        please check your JSON and try again.")
        |> render("index.html", user: user)
    end
  end
end
