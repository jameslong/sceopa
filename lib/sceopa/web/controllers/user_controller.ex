defmodule Sceopa.Web.UserController do
  use Sceopa.Web, :controller

  import Sceopa.Web.Plugs.Auth
  import Sceopa.Web.Plugs.User

  alias Sceopa.Accounts
  alias Sceopa.Blog

  plug :assign_user when action in [:show]
  plug :authenticate when action in [:delete]
  plug :ensure_user when action in [:delete]
  plug :ensure_unauthenticated when not action in [:delete, :show]

  def new(conn, _params) do
    changeset = Accounts.user_changeset()
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_account(user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "A confirmation email has been sent,\
          click the link inside to activate your account.")
        |> redirect(to: page_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def delete(conn, _params) do
    case Accounts.delete_account(conn.assigns.current_user) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Account deleted successfully.")
        |> redirect(to: page_path(conn, :index))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Account could not be deleted.")
        |> redirect(to: page_path(conn, :index))
    end
  end

  def show(conn, _params) do
    user = conn.assigns.user
    posts = Blog.user_posts(user)
    visible_posts = Blog.visible_posts(posts, conn.assigns.current_user)

    render(conn, "show.html", user: user, posts: visible_posts)
  end
end
