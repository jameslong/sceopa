defmodule Sceopa.Web.DashboardController do
  @moduledoc """
  Provides user dashboard behaviour.
  """

  use Sceopa.Web, :controller
  import Sceopa.Web.Plugs.Auth

  alias Sceopa.Blog

  plug :authenticate

  def index(conn, _) do
    user = conn.assigns.current_user
    posts = Blog.user_posts(user)
    render(conn, "index.html", user: user, posts: posts)
  end
end
