defmodule Sceopa.Web.PostController do
  @moduledoc """
  Provides user dashboard behaviour.
  """

  use Sceopa.Web, :controller
  import Sceopa.Web.Plugs.Auth
  import Sceopa.Web.Plugs.Post

  plug :authenticate when not action in [:show]
  plug :assign_post when action in [:edit, :update, :delete, :show]
  plug :ensure_post_author when action in [:edit, :update, :delete]
  plug :ensure_post_visible when action in [:show]

  alias Sceopa.Blog

  def new(conn, _) do
    changeset = Blog.post_changeset()
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}) do
    user = conn.assigns.current_user

    case Blog.insert_post(user, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created!")
        |> redirect(to: post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, _params) do
    post =
      conn.assigns.post
      |> Repo.preload(:user)

    render(conn, "show.html", post: post, user: post.user)
  end

  def edit(conn, _params) do
    post = conn.assigns.post
    changeset = Blog.post_changeset(post)
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"post" => post_params}) do
    post = conn.assigns.post

    case Blog.update_post(post, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, _params) do
    case Blog.delete_post(conn.assigns.post) do
      {:ok, _post} ->
        conn
        |> put_flash(:info, "Post deleted successfully.")
        |> redirect(to: dashboard_path(conn, :index))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Post could not be deleted.")
        |> redirect(to: dashboard_path(conn, :index))
    end
  end
end
