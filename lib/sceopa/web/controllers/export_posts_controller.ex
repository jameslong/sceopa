defmodule Sceopa.Web.ExportPostsController do
  @moduledoc """
  Provides action for downloading posts.
  """

  use Sceopa.Web, :controller
  import Sceopa.Web.Plugs.Auth
  import Sceopa.Web.Plugs.User

  alias Sceopa.Blog

  plug :authenticate
  plug :ensure_user

  def index(conn, _params) do
    posts =
      conn.assigns.current_user
      |> Blog.user_posts()

    render(conn, "index.json", posts: posts)
  end
end
