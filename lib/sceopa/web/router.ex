defmodule Sceopa.Web.Router do
  use Sceopa.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Sceopa.Web.Plugs.Auth, repo: Sceopa.Repo
  end

  scope "/", Sceopa.Web do
    pipe_through :browser

    get "/", PageController, :index

    get "/about", AboutController, :index

    get "/users/sign_in", SessionController, :new
    post "/users/sign_in", SessionController, :create
    get "/users/sign_out", SessionController, :delete

    resources "/users", UserController, only: [:new, :create, :delete, :show] do
      get "/posts/export", ExportPostsController, :index
      get "/posts/import", ImportPostsController, :index
      post "/posts/import", ImportPostsController, :update
    end

    get "/user/settings", UserSettingsController, :index
    get "/user/password", UserSettingsPasswordController, :edit
    put "/user/password", UserSettingsPasswordController, :update
    get "/user/username", UserSettingsUsernameController, :edit
    put "/user/username", UserSettingsUsernameController, :update

    get "/email/verify", EmailController, :index

    get "/dashboard", DashboardController, :index

    resources "/posts", PostController, except: [:index]
  end
end
