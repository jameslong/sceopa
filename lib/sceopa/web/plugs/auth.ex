defmodule Sceopa.Web.Plugs.Auth do
  @moduledoc """
  Provides authentication related plugs.
  """

  import Plug.Conn
  import Phoenix.Controller
  alias Sceopa.Accounts
  alias Sceopa.Auth
  alias Sceopa.Web.Router.Helpers

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, _repo) do
    user_id = get_session(conn, :user_id)
    user = user_id && Accounts.get_user(user_id)
    assign(conn, :current_user, user)
  end

  def authenticate(conn, _opts) do
    case Auth.authenticated?(conn) do
      true -> conn
      _ ->
        conn
        |> put_flash(:info, "You must be signed in to do that action.")
        |> redirect(to: Helpers.user_path(conn, :new))
        |> halt()
    end
  end

  def ensure_unauthenticated(conn, _opts) do
    case Auth.authenticated?(conn) do
      true ->
        conn
        |> put_flash(:info, "Only new visitors can do that action.")
        |> redirect(to: Helpers.page_path(conn, :index))
        |> halt()
      _ -> conn
    end
  end
end
