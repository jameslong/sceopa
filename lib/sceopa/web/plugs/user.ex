defmodule Sceopa.Web.Plugs.User do
  @moduledoc """
  Provides user-related plugs.
  """

  import Plug.Conn
  import Phoenix.Controller

  alias Sceopa.Accounts
  alias Sceopa.Web.Router.Helpers

  def assign_user(conn, _opts) do
    case Accounts.get_user(conn.params["id"]) do
      %Accounts.User{} = user ->
        assign(conn, :user, user)
      _ ->
        conn
        |> put_flash(:info, "User not found.")
        |> redirect(to: Helpers.page_path(conn, :index))
        |> halt()
    end
  end

  def ensure_user(%{params: %{"id" => id}} = conn, _opts) do
    check_user(conn, id)
  end
  def ensure_user(%{params: %{"user_id" => id}} = conn, _opts) do
    check_user(conn, id)
  end

  defp check_user(conn, id) do
    id = String.to_integer(id)
    user = conn.assigns.current_user
    if id === user.id do
      conn
    else
      conn
      |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end
end
