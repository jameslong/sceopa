defmodule Sceopa.Web.Plugs.Post do
  @moduledoc """
  Provides post-related plugs.
  """

  import Plug.Conn
  import Phoenix.Controller

  alias Sceopa.Blog
  alias Sceopa.Web.Router.Helpers

  def assign_post(conn, _opts) do
    case Blog.get_post(conn.params["id"]) do
      %Blog.Post{} = post ->
        assign(conn, :post, post)
      _ ->
        conn
        |> put_flash(:info, "Post not found.")
        |> redirect(to: Helpers.page_path(conn, :index))
        |> halt()
    end
  end

  def ensure_post_author(conn, _opts) do
    post = conn.assigns.post
    user = conn.assigns.current_user

    if Blog.author?(post, user) do
      assign(conn, :post, post)
    else
      conn
      |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end

  def ensure_post_visible(conn, _opts) do
    post = conn.assigns.post
    user = conn.assigns.current_user

    if Blog.visible?(post, user) do
      assign(conn, :post, post)
    else
      conn
      |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end
end
