defmodule Sceopa.Web.ExportPostsView do
  use Sceopa.Web, :view

  alias Sceopa.Blog

  def render("index.json", %{posts: posts}) do
    %{
      posts: Enum.map(posts, &Blog.to_json/1)
    }
  end
end
