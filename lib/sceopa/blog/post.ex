defmodule Sceopa.Blog.Post do
  @moduledoc "Model and behaviour for blog posts."

  use Sceopa.Web, :model

  schema "posts" do
    field :title, :string
    field :content, :string
    field :public, :boolean
    belongs_to :user, Sceopa.Accounts.User

    timestamps()
  end

  @max_title_length 200
  @max_content_length 10_000

  def changeset(model, params) do
    model
    |> cast(params, [:title, :content, :public, :inserted_at, :updated_at])
    |> validate_required([:title, :content, :public])
    |> validate_length(:title, min: 1, max: @max_title_length)
    |> validate_length(:content, min: 0, max: @max_content_length)
  end

  def to_json(post) do
    post
    |> Map.take([:title, :content, :public, :inserted_at, :updated_at])
  end
end
