defmodule Sceopa.Auth do
  @moduledoc "Context for auth related functionality."

  alias Plug.Conn
  alias Sceopa.Accounts

  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  def login(conn, user) do
    conn
    |> Conn.assign(:current_user, user)
    |> Conn.put_session(:user_id, user.id)
    |> Conn.configure_session(renew: true)
  end

  def logout(conn) do
    Conn.configure_session(conn, drop: true)
  end

  def authenticated?(conn) do
    conn.assigns.current_user != nil
  end

  def verify_pass(user, given_pass) do
    if user && checkpw(given_pass, user.password_hash) do
      {:ok, user}
    else
      {:error, :unauthorized}
    end
  end

  def login_by_email_and_pass(conn, email, given_pass) do
    user = Accounts.get_user_by_email(email)

    cond do
      user && checkpw(given_pass, user.password_hash) ->
        {:ok, login(conn, user)}
      user ->
        {:error, :unauthorized, conn}
      true ->
        dummy_checkpw()
        {:error, :not_found, conn}
    end
  end
end
